# docker-yml

## 介绍

集成常用的 docker compose yml配置文件

## 模块介绍

```
├─docekr-mongo
├─docker-ceph
├─docker-elk
│  ├─config
│  └─logstash
├─docker-fastdfs
├─docker-ipfs
├─docker-hyperledger
├─docker-java
├─docker-jemeter
├─docker-kafka
├─docker-monitor
│  ├─cadvisor
│  ├─grafana
│  ├─influxdb
│  ├─prometheus
│  └─telegraf
├─docker-mysql
├─docker-nginx
│  ├─conf
│  ├─conf.d
│  └─logs
├─docker-redis
├─docker-zabbix
└─docker-zookeeper
```

## 加入我们
* mail：guanxf@aliyun.com
* QQ Group:140586555。