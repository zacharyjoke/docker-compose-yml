### alert-manager：

https://github.com/alerta/prometheus-config.git

### 官方安装文档
https://prometheus.io/docs/guides/cadvisor/

### 插件

* https://grafana.com/dashboards/893
* https://grafana.com/dashboards/9276


### 说明

* node_exporter 负责采集硬件数据
* cAdvisor   负责采集容器数据
* prometheus 负责展示数据
* influxdbdata 负责存储数据

```xml
docker run -ti --rm -p 9308:9308 danielqsj/kafka-exporter --kafka.server=192.168.125.191:9092
```

### telegraf

收集硬件信息

https://github.com/jkehres/docker-compose-influxdb-grafana