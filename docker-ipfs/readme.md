
## 说明

IPFS which know as Interplanetary File System is peer to peer decentralized storage system which can be used share files(data) through a distributed file system. It made up of geographically distributed nodes where people and apps can storing and sharing files. IPFS supports to store files and track versions over time, very much like git. It uses content-addressing to uniquely identify the data in the network. Each file saved in the IPFS identified with its CID(content identifier) which derived through the hash of the file.

## 参考文档
* https://willschenk.com/articles/2019/setting_up_an_ipfs_node/
* https://medium.com/rahasak/ipfs-cluster-with-docker-db2ec20a6cc1
* https://gitlab.com/rahasak-labs/ipfs-ops