
## 镜像地址

* https://hub.docker.com/r/monitoringartist/zabbix-agent-xxl-limited

## 使用
### 执行
 zabbix-compose.yml

### 命令：
docker-compose -f zabbix-compose.yml  up -d

### * 访问地址
  <192.168.125.104> （Admin/zabbix)
  
 
 ```dockerfile
 
 docker run \
  --name=dockbix-agent-xxl \
  --net=host \
  --privileged \
  -v /:/rootfs \
  -v /var/run:/var/run \
  --restart unless-stopped \
  -e "ZA_Server=192.168.125.104" \
  -e "ZA_ServerActive=192.168.125.104" \
  -d monitoringartist/dockbix-agent-xxl-limited:latest
  
``` 
 
 
```dockerfile   
  docker run --rm \
  -e XXL_apiurl=http://zabbix.org/zabbix \
  -e XXL_apiuser=Admin \
  -e XXL_apipass=zabbix \
  monitoringartist/zabbix-templates
  ```

## zabbix 参考文档

* https://share.zabbix.com/virtualization/docker/docker-containers-monitoring?tdsourcetag=s_pcqq_aiomsg
* https://github.com/monitoringartist/dockbix-agent-xxl
* https://www.cnblogs.com/feigerlan/p/8193600.html